import { useNavigate } from "react-router-dom"

const LoginPages = () => {

    const navigate = useNavigate()

    const onLogin = () => {
        navigate('/', {
            replace: true
        })
    }

    return (
        <div className="container mt-5 mx-auto">
            <h1>Login</h1>
            <input type="text" />
            <button className="btn btn-primary" onClick={onLogin}>Login</button>
        </div>
    )
}

export default LoginPages