import HeroList from "../components/HeroList"

const DcPage = () => {
    return (
        <>
            <h1>DcPage</h1>
            <HeroList publisher='DC Comics' />
        </>
    )
}

export default DcPage