import { Link, NavLink, useNavigate } from 'react-router-dom';


export const Navbar = () => {

    const navigate = useNavigate()

    const onLogout = () => {
        navigate('/login')
    }

    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">

            <Link
                className="navbar-brand m-2"
                to="/"
            >
                Asociaciones
            </Link>
            <button className="navbar-toggler m-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="navbar-collapse mx-3" id='navbar'>
                <div className="navbar-nav">

                    <NavLink
                        className={({ isActive }) => `nav-item nav-link ${isActive ? 'active' : ''}`}
                        to="/marvel"
                    >
                        Marvel
                    </NavLink>

                    <NavLink
                        className={({ isActive }) => `nav-item nav-link ${isActive ? 'active' : ''}`}
                        to="/dc"
                    >
                        DC
                    </NavLink>

                    <div className='mt-3'>
                        <button
                            type='button'
                            className="nav-item nav-link btn"
                            onClick={onLogout}
                        >
                            Logout
                        </button>
                    </div>

                </div>
            </div>
        </nav>
    )
}